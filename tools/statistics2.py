import argparse
import os
import glob
import shutil
import json
import re
import hashlib
import numpy as np
import sys
import imgaug as ia
import random

from imgaug import augmenters as iaa
from multiprocessing import Pool, Value, Manager
from lxml import etree
from PIL import Image, ImageFile, ImageDraw, ImageFont
ImageFile.LOAD_TRUNCATED_IMAGES = True

parser = argparse.ArgumentParser()

parser.add_argument("--input_dir", help="path to folder containing app packages", default="android_data_ambig_aug2_pb2")
a = parser.parse_args()

inputDir = a.input_dir
dir_img = os.path.join(inputDir, "PNGImages")
dir_set = os.path.join(inputDir, "ImageSets")
dir_ann = os.path.join(inputDir, "Annotations")

input_file = os.path.join(dir_set, "processed.txt")

files = []
befores = []
afters = []
names = []
src_dir = ""

with open(input_file, 'r') as f:
    for line in f.readlines():
        strs = line.split(", [")
        infos = strs[0].split(",")

        file_dir = infos[0]
        before = int(infos[1])
        after = int(infos[2])
        #if file_dir == "../../data/data5000/verbo1/ali.alhadidi.gif_facebook_16-output":
        #if file_dir == "../../data/data5000/verbo1/app.ym.sondakika_66-output":
        if file_dir == "../../data/data5000/verbo1/ar.com.agea.gdt_61-output":
            names = strs[1].replace("'","").replace(" ", "").replace("]\n", "").split(",")
            src_dir = file_dir
            print(before, ":", after)

        files.append(file_dir)
        befores.append(before)
        afters.append(after)

print(names)
dest_dir = os.path.join(".", "test")
if os.path.exists(dest_dir):
    shutil.rmtree(dest_dir)
os.makedirs(dest_dir)
for n in names:
    f = n + ".png"
    src = os.path.join(src_dir, "stoat_fsm_output", "ui", f)
    #dest = os.path.join(dest_dir, n)
    shutil.copy(src,  dest_dir)


files = np.array(files)
befores = np.array(befores)
afters = np.array(afters)

median_b = np.median(befores)
median_a = np.median(afters)

average_b = np.average(befores)
average_a = np.average(afters)

print("-------------------")
print('median:  before:', median_b, ' after:', median_a)
print('average:  before:', average_b, ' after:', average_a)

#'''
#fname = os.path.join("android_data", "Annotations", "000001.txt")
#with open(fname) as f:
#    datastore = json.load(f)
#    bb_from = datastore[0]['coordinates']['from']
#    print bb_from, bb_from[0]
#'''
#
##folders_list = folders_list[:200]
##print("folders_list", folders_list)
#
#files_dict = dict()
#for app_folder in folders_list:
#    xmls = glob.glob(app_folder + "/stoat_fsm_output/ui/*.xml")
#    pngs = glob.glob(app_folder + "/stoat_fsm_output/ui/*.png")
#    files_dict[app_folder] = len(pngs)
#
##tmp = np.array(files_dict.values())
##print(tmp)
#values = list(files_dict.values())
#values = np.array(values)
#
#print(len(folders_list), "apps having", values.sum(), "pics")
#
#median = np.median(values)
#average = np.average(values)
#print('median:', median)
#print('average:', average)
#
#sorted_number = sorted(values, reverse=True)
##print(sorted_number[:20])
#sorted_number = np.array(sorted_number)
#
#gt1000 = values[values > 1000]
#gt500 = values[values > 500]
#gt200 = values[values > 200]
#print("greater than 1000:", len(gt1000), " having ", np.array(gt1000).sum(), "pics")
#print("greater than 500:", len(gt500), " having ", np.array(gt500).sum(), "pics")
#print("greater than 200:",len(gt200))
#
#top1000 = sorted_number[:1000]
#print("there are 1000 apps having larger than", np.min(top1000), "pics")
#
#
#invalid = values[values == 0]
#print("have no screenshots:", len(invalid))
#print("-------------------")
#
#valid = []
#for i, v in enumerate(values):
#    if v not in gt1000 and v not in invalid:
#        valid.append(i)
#print("after remove the ones larger than 1000 and having no pics:", len(valid))
#
#values = values[valid]
#median = np.median(values)
#average = np.average(values)
#print('median:', median)
#print('average:', average)
#
#print("-------------------")
#valid = []
#for i, v in enumerate(values):
#    if v not in gt500 and v not in invalid:
#        valid.append(i)
#print("after remove the ones larger than 500 and having no pics:", len(valid))
#
#values = values[valid]
#median = np.median(values)
#average = np.average(values)
#print('median:', median)
#print('average:', average)
