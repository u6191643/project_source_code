1. tools/demo_modified.py : this is for generating the visible detection demo with detection boundary and score


1. tools/demo_modified_xml.py : this is for generating detection results while generating the xml file with the same structure as the one in data500 that is compatible processed by widget clipping 


1. train : the script for commands of training faster RCNN

1. tf-faster-rcnn/output/res101/android_train/default/* : these files are the latest trained module which can be used directly
1. tf-faster-rcnn/experiments/logs/* : these are the latest training and testing logs
1. tf-faster-rcnn/experiments/cfgs/res101.yml : this is the configuration file of training
