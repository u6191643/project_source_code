import argparse
import glob
import io
import json
import os
import os.path as osp
import re
import shutil
import time
import sys
import requests

#from urllib.request import urlopen
from PIL import Image
from multiprocessing import Pool, Value
from tqdm import tqdm
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", required=True, help="path to folder containing Play Store json files", default='data/play_store_json')
parser.add_argument("--output_dir", help="directory to store output screenshots", default='data')
a = parser.parse_args()

OUTPUT_DIR = "play_store_screenshots"

developers = defaultdict(set)
developers_number = dict()

def scrape_parallel(file):
    global count

    with open(file) as f:
        datastore = json.load(f)
        for data in datastore:
            url = data['Url'].split('=')[-1]
            dev = data['Developer']
            developers[dev].add(url)
            developers_number[dev] = len(developers[dev])
            print(url, ':', dev)

    print(developers)
    print("====")
    print(developers_number)
    big_com = []
    companies = sorted(developers_number.items(), key=lambda item:item[1], reverse=True)
    for item in companies:
        if item[1] < 2:
            big_com.append(item)

    print(big_com)

def init(c):
    global count
    count = c


if __name__ == "__main__":

    if a.output_dir is not None:
        OUTPUT_DIR = osp.join(a.output_dir, OUTPUT_DIR)

    if osp.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)
    os.makedirs(OUTPUT_DIR)

    files = glob.glob(a.input_dir + "/*.json")
    folders = glob.glob(a.input_dir + "/*20170510_cleaned*/**")
    meta_dump = {}
    
    count = Value('i', 0)

    start = time.time()
    for file in files:
        scrape_parallel(file)

    print("[+] Completed in {} seconds".format(time.time()-start))

