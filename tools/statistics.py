import argparse
import os
import glob
import shutil
import json
import re
import hashlib
import numpy as np
import sys
import imgaug as ia
import random

from imgaug import augmenters as iaa
from multiprocessing import Pool, Value, Manager
from lxml import etree
from PIL import Image, ImageFile, ImageDraw, ImageFont
ImageFile.LOAD_TRUNCATED_IMAGES = True

parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", required=False, help="path to folder containing app packages", default='../../data/data5000/')
a = parser.parse_args()

folders_list = glob.glob(a.input_dir + "verbo*/*")
folders_list = np.array(folders_list)

'''
fname = os.path.join("android_data", "Annotations", "000001.txt")
with open(fname) as f:
    datastore = json.load(f)
    bb_from = datastore[0]['coordinates']['from']
    print bb_from, bb_from[0]
'''

#folders_list = folders_list[:200]
#print("folders_list", folders_list)

files_dict = dict()
for app_folder in folders_list:
    xmls = glob.glob(app_folder + "/stoat_fsm_output/ui/*.xml")
    pngs = glob.glob(app_folder + "/stoat_fsm_output/ui/*.png")
    files_dict[app_folder] = len(pngs)

#tmp = np.array(files_dict.values())
#print(tmp)
values = list(files_dict.values())
values = np.array(values)

print(len(folders_list), "apps having", values.sum(), "pics")

median = np.median(values)
average = np.average(values)
print('median:', median)
print('average:', average)

sorted_number = sorted(values, reverse=True)
#print(sorted_number[:20])
sorted_number = np.array(sorted_number)

gt1000 = values[values > 1000]
gt500 = values[values > 500]
gt200 = values[values > 200]
print("greater than 1000:", len(gt1000), " having ", np.array(gt1000).sum(), "pics")
print("greater than 500:", len(gt500), " having ", np.array(gt500).sum(), "pics")
print("greater than 200:",len(gt200), " having ", np.array(gt200).sum(), "pics")

top1000 = sorted_number[:1000]
print("there are 1000 apps having larger than", np.min(top1000), "pics")


invalid = values[values == 0]
print("have no screenshots:", len(invalid))
print("-------------------")

valid = []
for i, v in enumerate(values):
    if v not in gt1000 and v not in invalid:
        valid.append(i)
print("after remove the ones larger than 1000 and having no pics:", len(valid))

values = values[valid]
median = np.median(values)
average = np.average(values)
print('median:', median)
print('average:', average)

print("-------------------")
valid = []
for i, v in enumerate(values):
    if v not in gt500 and v not in invalid:
        valid.append(i)
print("after remove the ones larger than 500 and having no pics:", len(valid))

values = values[valid]
median = np.median(values)
average = np.average(values)
print('median:', median)
print('average:', average)
