import argparse
import os
import glob
import shutil
import json
import re
import hashlib
import numpy as np
import sys
import imgaug as ia
import random
import time
import pdb

from imgaug import augmenters as iaa
from multiprocessing import Pool, Value, Manager
from lxml import etree
from PIL import Image, ImageFile, ImageDraw, ImageFont
from tqdm import tqdm
ImageFile.LOAD_TRUNCATED_IMAGES = True

parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", help="path to folder containing app packages", default='../../data/data5000/')
a = parser.parse_args()

outputDir = "android_data_ambig_aug2_pb7_3"
input_subdir = "verbo3/*"


# outputDir = "test"
dir_img = os.path.join(outputDir, "PNGImages")
dir_set = os.path.join(outputDir, "ImageSets")
dir_ann = os.path.join(outputDir, "Annotations")

target_list = ["EditText", "TextView", "Button", "ImageButton", "CompoundButton", "ProgressBar", "SeekBar", "Chronometer", "CheckBox", "RadioButton", "Switch", "ToggleButton", "RatingBar", "Spinner",] # "View"]
# target_list = ["TextView", "Button", "ImageButton", "SeekBar", "CheckBox", "RadioButton", "EditText",]


rare_targets = {}
rare_targets["Chronometer"] = []
rare_targets["CompoundButton"] = []
rare_targets["Spinner"] = []
rare_targets["RatingBar"] = []
#rare_targets["CompoundButton"] = []
#rare_targets["Switch"] = []
#rare_targets["SeekBar"] = []

redundant_targets = {}
redundant_targets["Button"] = []
redundant_targets["ImageButton"] = []
test_list = []

def checkFileValidity(inputFile):
    '''
    Check the validity of the XML file and ignore it if possible
    Due to the unknown reasons, the content in some XML file is repetative or   
    '''
    homeScreen_list = ["Make yourself at home", "You can put your favorite apps here.", "To see all your apps, touch the circle."]
    unlockHomeScreen_list = ["Camera", "[16,600][144,728]", "Phone", "[150,1114][225,1189]", "People", "[256,1114][331,1189]", "Messaging", "[468,1114][543,1189]", "Browser", "[574,1114][649,1189]"]
    browser = ["com.android.browser:id/all_btn", "[735,108][800,172]", "com.android.browser:id/taburlbar", "com.android.browser:id/urlbar_focused"]
    with open(inputFile) as f:
        content = f.read()
        #it is the layout code for the whole window and no rotation
        if 'bounds="[0,0][800,1216]"' in content and '<hierarchy rotation="1">' not in content:
            if not all(keyword in content for keyword in browser) and not all(keyword in content for keyword in homeScreen_list) and not all(keyword in content for keyword in unlockHomeScreen_list):
                #it should not be the homepage of the phone
                bounds_list = re.findall(r'bounds="(.+?)"', content)
                if len(bounds_list) < 2:
                    return False
                #if float(len(bounds_list)) / len(set(bounds_list)) < 1.2:   #so far, we do not check this option
                    #print len(text_list), len(set(text_list)), inputFile.split("\\")[-1]
                return True
            
    return False        


def getDimensions(coor_from, coor_to):
    dim = {}
    dim['width'] = coor_to[0] - coor_from[0]
    dim['height'] = coor_to[1] - coor_from[1]
    return dim


def remove_overlap(widgets, status_bar):
    global countValidFile, train_val
    w = []
    widgets.reverse()
    layout = np.zeros((801,1217), dtype=np.int)
    for idx, widget in enumerate(widgets):
        overlap = False
        starti = widget['coordinates']['from'][0] + 1
        endi = widget['coordinates']['to'][0] - 1
        startj = widget['coordinates']['from'][1] + 1
        endj = widget['coordinates']['to'][1] - 1

        for i in range(starti, endi + 1):
            for j in range(startj, endj + 1):
                if layout[i][j] == 1:
                    if widget['leaf']:
                        overlap = True
                        layout[i][j] = -1
                elif layout[i][j] == -1:
                    overlap = True
                elif layout[i][j] == 2:
                    if widget['leaf']:
                        overlap = True
                        layout[i][j] = -1
                else:
                    if widget['leaf']:
                        layout[i][j] = 1
                    else:
                        layout[i][j] = 2

        if overlap == True:
            continue
        else:
            if widget['leaf'] and widget['widget_class'] in target_list:
                if widget['widget_class'] == "View" and not(widget['clickable'] == "true" and widget['focusable'] == "true"):
                    continue

                w.append(widget)

    if w:
        with countValidFile.get_lock():
            countValidFile.value += 1
        
            try:
                im = Image.open(w[0]['src']+'.png')
                if status_bar:
                    clip = im.crop((0, 33, 800, 1216))
                else:
                    clip = im.crop((0, 0, 800, 1216))
            except OSError as err:
                print(w[0]['src'])
                print("[-] OSError - " + str(err))
                sys.stdout.flush()
                pass
            except IndexError as err:
                print(w[0]['src'])
                print("[-] IndexError - " + str(err))
                sys.stdout.flush()
                #print "[-] " + str(widget['coordinates'])
                #print "[-] " + str(widget['dimensions'])
                pass
            except IOError as err: #image file is truncated
                print(w[0]['src'])
                print("[-] IOError - " + str(err))
                sys.stdout.flush()
                pass

            clip.save(os.path.join(dir_img, "{0:0>6}.png".format(countValidFile.value)))
            if countValidFile.value % 30 == 1:
                val = train_val['val']
                val.append("{:06d}".format(countValidFile.value))
                train_val['val'] = val
            else:
                train = train_val['train']
                train.append("{:06d}".format(countValidFile.value))
                train_val['train'] = train

            trainval = train_val['trainval']
            trainval.append("{:06d}".format(countValidFile.value))
            train_val['trainval'] = trainval

                
            with open(os.path.join(dir_ann, "{0:0>6}.txt".format(countValidFile.value)), 'a+') as f:
                #tmp = {}
                #tmp['app'] = w
                json.dump(w, f, sort_keys=True, indent=3, separators=(',', ': '))

                    #with open(os.path.join(dir_ann, "{0:0>6}.txt".format(countValidFile.value)), 'a+') as f:
                    #    json.dump(widget, f, sort_keys=True, indent=3, separators=(',', ': '))


def compareHisto(first, sec):
    imA = Image.open(first)
    imB = Image.open(sec)

    # Normalise the scale of images 
    if imA.size[0] > imB.size[0]:
        imA = imA.resize((imB.size[0], imA.size[1]))
    elif imA.size[0] < imB.size[0]:
        imB = imB.resize((imA.size[0], imB.size[1]))

    if imA.size[1] > imB.size[1]:
        imA = imA.resize((imA.size[0], imB.size[1]))
    elif imA.size[1] < imB.size[1]:
        imB = imB.resize((imB.size[0], imA.size[1]))

    hA = imA.histogram()
    hB = imB.histogram()
    sum_hA = 0.0
    sum_hB = 0.0
    diff = 0.0

    for i in range(len(hA)):
        #print(sum_hA)
        sum_hA += hA[i]
        sum_hB += hB[i]
        diff += abs(hA[i] - hB[i])

    res = diff/(2*max(sum_hA, sum_hB))
    return res

def rem(ann):
    ann['coordinates']['from'] = list(ann['coordinates']['from'])
    ann['coordinates']['to'] = list(ann['coordinates']['to'])
    ann['coordinates']['from'][1] = ann['coordinates']['from'][1] - 33
    ann['coordinates']['to'][1] = ann['coordinates']['to'][1] - 33
    ann['coordinates']['from'] = tuple(ann['coordinates']['from'])
    ann['coordinates']['to'] = tuple(ann['coordinates']['to'])
    return ann

def augment(img, anns, is_test, frame_dict):
    global stats
    width, height = img.size
    ori = img
    img = np.array(img, dtype=np.uint8)
    valid = []

    kps = []
    for a in anns:
        x1 = a['coordinates']['from'][0] 
        y1 = a['coordinates']['from'][1] 
        x2 = a['coordinates']['to'][0] 
        y2 = a['coordinates']['to'][1] 
        kps.extend([ia.Keypoint(x=x1, y=y1), ia.Keypoint(x=x2, y=y2),])
        stats[a['widget_class']] += 1

    keypoints = ia.KeypointsOnImage(kps, shape=img.shape)

    '''
    seq = iaa.SomeOf((1, None), [
        # iaa.Fliplr(1.0),
        iaa.AverageBlur(k=2),
        iaa.Sometimes(
            0.5,
            iaa.CropAndPad(percent=(0, 0.25),
                           pad_mode=["constant"],
                           pad_cval=(0, 128)),
            iaa.CropAndPad(
                px=((0, 100), 100, (0, 100), 100),
                pad_cval=(0, 128)
            )
        )

    ])
    
    seq = iaa.Sometimes(
        0.1,
        iaa.Fliplr(1.0),
        iaa.CropAndPad(percent=(0, 0.25))
    )

    '''

    probability = random.uniform(0., 1.)

    if probability > 0.2:
        scale = random.uniform(0.6, 0.9)

        tran_num = (1 - scale) / 2
        trans_per = random.uniform(-tran_num/2, tran_num)
        # color = random.randrange(0,250)
        color = ia.ALL

        seq = iaa.Sequential([
            iaa.Affine(scale=scale, translate_percent={"y": trans_per}, cval=color),
                              ])

        seq_det = seq.to_deterministic()

        #augment keypoints and images
        # im = Image.fromarray(img)
        # im.show()
        img_aug = seq_det.augment_images([img])[0]
        keypoints_aug = seq_det.augment_keypoints([keypoints])[0]

    else:
        index = random.randint(1, 5)
        frame_dir = "./mobile_frame"
        frame_name = str(index) + ".png"
        frame_addr = os.path.join(frame_dir, frame_name)
        frame = Image.open(frame_addr)

        w, h = frame_dict[frame_name][0]
        x, y = frame_dict[frame_name][1]
        seq = iaa.Sequential([
            iaa.Scale({"height": h, "width": w}),
            iaa.Affine( translate_px={"x": x, "y": y}),
        ])
        seq_det = seq.to_deterministic()

        #augment keypoints and images
        # im = Image.fromarray(img)
        # im.show()

        # img_aug = seq_det.augment_images([img])[0]
        keypoints_aug = seq_det.augment_keypoints([keypoints])[0]

        ori = ori.resize((w, h), resample=Image.LANCZOS)
        frame = Image.open(frame_addr)
        frame.paste(ori, (x, y))
        # frame.show()
        img_aug = np.array(frame, dtype=np.uint8)


    # im = Image.fromarray(img_aug)
    # im.show()

    aug_anns = []
    #for i, value in range(len(anns)):
    for i, value in enumerate(anns):
        '''
        if keypoints_aug.keypoints[i*2].x == -1:
            keypoints_aug.keypoints[i*2].x = 0
        if keypoints_aug.keypoints[i*2+1].x == -1:
            keypoints_aug.keypoints[i*2+1].x = 0
        '''
        if keypoints_aug.keypoints[i*2].x > keypoints_aug.keypoints[i*2+1].x:
            temp = keypoints_aug.keypoints[i*2].x
            keypoints_aug.keypoints[i*2].x = keypoints_aug.keypoints[i*2+1].x
            keypoints_aug.keypoints[i*2+1].x = temp

        if keypoints_aug.keypoints[i*2].x < 0:
            keypoints_aug.keypoints[i*2].x = 0
        if keypoints_aug.keypoints[i*2].x > width:
            keypoints_aug.keypoints[i*2].x = width
        if keypoints_aug.keypoints[i*2+1].x < 0:
            keypoints_aug.keypoints[i*2+1].x = 0
        if keypoints_aug.keypoints[i*2+1].x > width:
            keypoints_aug.keypoints[i*2+1].x = width
        if keypoints_aug.keypoints[i*2].y < 0:
            keypoints_aug.keypoints[i*2].y = 0
        if keypoints_aug.keypoints[i*2].y > height:
            keypoints_aug.keypoints[i*2].y = height
        if keypoints_aug.keypoints[i*2+1].y < 0:
            keypoints_aug.keypoints[i*2+1].y = 0
        if keypoints_aug.keypoints[i*2+1].y > height:
            keypoints_aug.keypoints[i*2+1].y = height

        anns[i]['dimensions'] = getDimensions((keypoints_aug.keypoints[i*2].x, keypoints_aug.keypoints[i*2].y),
                                              (keypoints_aug.keypoints[i*2+1].x, keypoints_aug.keypoints[i*2+1].y))
        if anns[i]['dimensions']['width'] == 0 or anns[i]['dimensions']['height'] == 0:
            continue
        anns[i]['coordinates']['from'] = (keypoints_aug.keypoints[i*2].x, keypoints_aug.keypoints[i*2].y)
        anns[i]['coordinates']['to'] = (keypoints_aug.keypoints[i*2+1].x, keypoints_aug.keypoints[i*2+1].y)
        valid.append(i)

    aug_anns = []
    for idx in valid:
        aug_anns.append(anns[idx])

    im = Image.fromarray(img_aug)
    # draw = ImageDraw.Draw(im)
    '''
    for a in aug_anns:
        draw.rectangle((a['coordinates']['from'], a['coordinates']['to']), outline="red")
    '''
    '''
    for i in range(0,len(keypoints.keypoints),2):
        before = keypoints.keypoints[i]
        after = keypoints_aug.keypoints[i]
        print "Keypoint %d: (%.8f, %.8f) -> (%.8f, %.8f)" % (i, before.x, before.y, after.x, after.y)
        draw.rectangle((keypoints_aug.keypoints[i].x, keypoints_aug.keypoints[i].y, keypoints_aug.keypoints[i+1].x, keypoints_aug.keypoints[i+1].y), outline="red")
    im.show()
    '''

    if aug_anns:
        # train_test_split(im, aug_anns, True)
        return train_test_store(im, aug_anns, True, is_test)
    else:
        return False


def train_test_store(clip, anns, aug=False, is_test=False):
    global countValidFile, train_val

    with countValidFile.get_lock():
        countValidFile.value += 1
        count = countValidFile.value

        name = "c{:06d}".format(count)
        if is_test:
            val = train_val['val']
            val.append(name)
            train_val['val'] = val
        else:
            train = train_val['train']
            train.append(name)
            train_val['train'] = train

        trainval = train_val['trainval']
        trainval.append(name)
        train_val['trainval'] = trainval

        txt_name = os.path.join(dir_ann, "c{0:0>6}.txt".format(count))
        with open(txt_name, 'a+') as f:
            json.dump(anns, f, sort_keys=True, indent=3, separators=(',', ': '))

        pic_name = os.path.join(dir_img, "c{0:0>6}.png".format(count))
        clip.save(pic_name)

        return (name, pic_name, txt_name)

def train_test_split(clip, anns):
    global countValidFile, train_val
    is_test = False
    aug = True

    with countValidFile.get_lock():
        countValidFile.value += 1
        count = countValidFile.value

        to_aug = random.uniform(0., 1.)
        # if to_aug < 0.7:
        #     aug = True

        if count % 15 == 1:
            val = train_val['val']
            val.append("{:06d}".format(count))
            train_val['val'] = val
            is_test = True
        else:
            train = train_val['train']
            train.append("{:06d}".format(count))
            train_val['train'] = train
            is_test = False

        trainval = train_val['trainval']
        trainval.append("{:06d}".format(count))
        train_val['trainval'] = trainval

        file_name = os.path.join(dir_ann, "{0:0>6}.txt".format(count))
        with open(file_name, 'a+') as f:
            json.dump(anns, f, sort_keys=True, indent=3, separators=(',', ': '))

        clip.save(os.path.join(dir_img, "{0:0>6}.png".format(count)))

        for ann in anns:
            if ann["widget_class"] == "Button":
                redundant_targets["Button"].append(file_name)
            elif ann["widget_class"] == "ImageButton":
                redundant_targets["ImageButton"].append(file_name)
            elif ann["widget_class"] == "Chronometer":
                rare_targets["Chronometer"].append(file_name)
            elif ann["widget_class"] == "CompoundButton":
                rare_targets["CompoundButton"].append(file_name)
            elif ann["widget_class"] == "Spinner":
                rare_targets["Spinner"].append(file_name)

        return aug

def preprocess(input_folder):
    global countValidFile, train_val, test_list
    pnglist = []
    is_test_data = False
    if input_folder in test_list:
        is_test_data = True

    frame_dict = dict()
    frame_dict["1.png"] = ((352, 624), (75, 140))  # ((w,h),(x,y))
    frame_dict["2.png"] = ((438, 624), (56, 140))  # ((w,h),(x,y))
    frame_dict["3.png"] = ((413, 624), (50, 127))  # ((w,h),(x,y))
    frame_dict["4.png"] = ((437, 694), (28, 121))  # ((w,h),(x,y))
    frame_dict["5.png"] = ((335, 524), (11, 55))  # ((w,h),(x,y))


    run_time = 0
    n_only_buttons = 0
    iterater = glob.glob(input_folder + "/stoat_fsm_output/ui/*.xml")
    l = len(iterater)
    file_dict = dict()
    redundent_dict = dict()
    rare_set = set()
    # print('l:',l)
    for index, infile in enumerate(glob.glob(input_folder + "/stoat_fsm_output/ui/*.xml")):
        widgets_xml = []
        status_bar = False
        name, ext = os.path.splitext(infile)
        pngfile = infile.replace('.xml', '.png')
        
        only_button = True
        widgets_num = 0
        # if infile == "../../data5000/top_10000_google_play_20170510_cleaned_outputs_verbose_xml/incomeincloud.android.activity_246-output/stoat_fsm_output/ui/S_1126.xml":
        #     print("-----------")

        if os.path.exists(pngfile) and os.stat(pngfile).st_size > 0 and checkFileValidity(infile):
            try:
                Image.open(pngfile)
            except Exception as e:
                print(e)
                sys.stdout.flush()
                continue

            #print(pngfile)


            is_rare = False
            ctx = etree.iterparse(infile, events=('start',), tag='node')
            attr_list = []
            for event, elem in ctx:
                widget_name = elem.attrib['class'].split('.')[-1]
                if widget_name in rare_targets.keys():
                    is_rare = True
                if widget_name in target_list:
                    widgets_num += 1                    
                    attr_list.append(elem.attrib)

                    if widget_name not in redundant_targets.keys():
                        only_button = False
            
            #if widgets_num == 0:
            #    continue

            class_list = []
            if not is_rare:
                for attr in attr_list:
                    class_list.append((attr['class'], attr['bounds']))
                attr_list = class_list

            # check for duplicate image
            dup = False
            if not pnglist:
                pnglist.append(pngfile)
            else:
                #tic = time.clock()
                #for png in pnglist:
                #    diff_score2 = compareHisto(pngfile, png)
                #    if is_rare:
                #        if diff_score2 < 0.020:
                #            diff_score1 = compare_locations(attr_list, png, is_rare)
                #            if (diff_score1 > 0.94):
                #                dup = True
                #                break
                #    else:
                #        if diff_score2 < 0.050:
                #            diff_score1 = compare_locations(attr_list, png, is_rare)
                #            if (diff_score1 > 0.80):
                #                dup = True
                #                break
                for png in pnglist:
                    diff_score1 = compare_locations(attr_list, png, is_rare)
                    if is_rare:
                        if (diff_score1 > 0.94):
                            diff_score2 = compareHisto(pngfile, png)
                            if diff_score2 < 0.020:
                                dup = True
                                break
                    else:
                        if (diff_score1 > 0.80):
                            diff_score2 = compareHisto(pngfile, png)
                            if diff_score2 < 0.050:
                                dup = True
                                break
                #toc = time.clock()
                #run_time += toc-tic
                if dup:
                    continue
                else:
                    pnglist.append(pngfile)

            ctx = etree.iterparse(infile, events=('start',), tag='node')
            for event, elem in ctx:
                # Check for android status bar
                if elem.attrib['bounds'] == "[0,33][800,1216]": 
                    status_bar = True

                widget_name = elem.attrib['class'].split('.')[-1]
                coordinates = re.findall(r"(?<=\[).*?(?=\])", elem.attrib["bounds"])
                if len(coordinates) != 2: 
                    continue

                #if not widget_name in target_list:
                #    continue

                #if not widget_name in layout and len(elem.getchildren()) != 0:
                #    continue

                children = elem.getchildren()
                # if widget_name == "Spinner":
                # if widget_name == "CompoundButton":
                #     print("!!!!!!!!!!", len(children))

                if widget_name not in target_list:
                    continue

                # if widget_name != "Spinner" and len(children) != 0:
                if not widget_name in target_list or len(elem.getchildren()) != 0:
                    continue

                coor_from = tuple(map(int, coordinates[0].split(",")))
                coor_to = tuple(map(int, coordinates[1].split(",")))

                if not (coor_from[0] > 800 or coor_from[0] < 0 or
                                coor_to[0] > 800 or coor_to[0] < 0 or
                                coor_from[1] > 1216 or coor_from[1] < 0 or
                                coor_to[1] > 1216 or coor_to[1] < 0):
                    meta_data = {}
                    meta_data['widget_class'] = widget_name

                    meta_data['coordinates'] = {'from': coor_from, 'to': coor_to}
                    meta_data['dimensions'] = getDimensions(coor_from, coor_to)
                    if meta_data['dimensions']['width'] == 0 or meta_data['dimensions']['height'] == 0:
                        continue

                    if (meta_data['widget_class'] == "TextView" and
                            not(len(elem.attrib['text'].split(" ")) <= 3 and
                                    not(elem.attrib['checkable'] == "true") and
                                    not(elem.attrib['checked'] == "true") and
                                        elem.attrib['clickable']  == "true" and
                                        elem.attrib['enabled']  == "true" and
                                        elem.attrib['focusable']  == "true" and
                                    not(elem.attrib['focused'] == "true") and
                                    not(elem.attrib['scrollable'] == "true") and
                                        elem.attrib['long-clickable'] == "true")):
                        continue

                    # Classify ProgressBar(PB) as circular/horizontal based on aspect ratio, remaining regarded as invalid 
                    # Circular PB:         aspect_ratio == 1
                    # Horizontal PB:    aspect_ratio < 0.3
                    '''
                    if meta_data['widget_class'] == "ProgressBar":
                        ar = float(meta_data['dimensions']['height']) / float(meta_data['dimensions']['width'])
                        if ar == 1:
                            meta_data['widget_class'] = "CirProgressBar"
                        elif ar < 0.3:
                            meta_data['widget_class'] = "HrzProgressBar"
                        else:
                            widgets_xml = []
                            break
                    '''

                    meta_data['text'] = elem.attrib['text'] 
                    meta_data['clickable'] = elem.attrib['clickable']
                    meta_data['focusable'] = elem.attrib['focusable']
                    meta_data["content-desc"] = elem.attrib['content-desc']
                    meta_data['src'] = name

                    #if meta_data['widget_class'] == "TextView" or meta_data['widget_class'] in layout:
                    if meta_data['widget_class'] == "TextView":
                        meta_data['widget_class'] = "ImageButton"

                    widgets_xml.append(meta_data)

        else:
            continue

        if widgets_xml:        
            try:
                im = Image.open(pngfile)
                if status_bar:
                    clip = im.crop((0, 32, 800, 1216))
                    w = [rem(ann) for ann in widgets_xml if not (ann['coordinates']['from'][1] < 33 or ann['coordinates']['to'][1] < 33)]

                else:
                    clip = im.crop((0, 0, 800, 1216))
                    w = widgets_xml
            except OSError as err:
                print(pngfile)
                print("[-] OSError - " + str(err))
                sys.stdout.flush()
                continue
            except IndexError as err:
                print(pngfile)
                print("[-] IndexError - " + str(err))
                sys.stdout.flush()
                continue
            except IOError as err: #image file is truncated
                print(pngfile)
                print("[-] IOError - " + str(err))
                sys.stdout.flush()
                continue

            with countValidFile.get_lock():
                file_dict[pngfile] = []
                # Train/Test split for augmented dataset
                # aug = train_test_split(clip, w, is_test_data)
                saved_pic = train_test_store(clip, w, is_test=is_test_data)
                file_dict[pngfile].append(saved_pic)

                aug_result = augment(clip, w, is_test_data, frame_dict)
                if aug_result != False:
                    file_dict[pngfile].append(aug_result)
                #print(file_dict)

                if widgets_num != 0:
                    redundent_dict[pngfile] = only_button
                    if is_rare:
                        rare_set.add(pngfile)
                    n_only_buttons += 1

    #if len(pnglist) != 0:
    #    print("runtime:", run_time, " list_len:", len(pnglist), " averagetime:", run_time/len(pnglist))

    #pdb.set_trace()

    rm_list = []
    reserv_num = 24
    if len(pnglist) > reserv_num:
        with countValidFile.get_lock():
            #print(rare_set)
            for png in pnglist:
                r1 = random.uniform(0., 1.)
                if r1 > reserv_num / len(pnglist):
                    if png not in rare_set:
                        rm_list.append(png)

            #print("==============", len(pnglist))
            #print(pnglist)
            #print("~~~~~~~~~~~~~~", len(rm_list))
            #print(rm_list)

            for rm in rm_list:
                trainval = train_val['trainval']
                val = train_val['val']
                train = train_val['train']

                if rm in file_dict.keys():
                    gen_list = file_dict[rm]
                    for gen in gen_list:
                        name = gen[0]
                        png = gen[1]
                        txt = gen[2]

                        #print(trainval)
                        #print("=====",name)
                        trainval.remove(name)
                        train_val['trainval'] = trainval
                        if name in train:
                            train.remove(name)
                            train_val['train'] = train
                            os.remove(png)
                            os.remove(txt)
                        elif name in val:
                            val.remove(name)
                            train_val['val'] = val
                            os.remove(png)
                            os.remove(txt)
                        else:
                            print("!!!!!!!!! something wrong !!!!!! about", name)

                    #print('remove '+ name + "------"+ png+ "------"+ txt)

                pnglist.remove(rm)

                #print("~~~~~~~~~~~~~~", len(pnglist))
                #print(pnglist)
                #print("==============")

    return input_folder, l, len(pnglist), pnglist

def compare_locations(L1, png2, is_rare):
    xml2 = png2.replace('.png', '.xml')
    ctx = etree.iterparse(xml2, events=('start',), tag='node')
    L2 = []
    for event, elem in ctx:
        widget_name = elem.attrib['class'].split('.')[-1]
        if widget_name not in target_list:
            continue

        if is_rare:
            L2.append(elem.attrib)
        else:
            L2.append((elem.attrib['class'], elem.attrib['bounds']))

    same_widgets = 0
    if len(L1) <= len(L2):
        if len(L1) == 0:
            return 1.0

        for item in L1:
            if item in L2:
                same_widgets += 1
        diff = float(same_widgets) / len(L1)

    elif len(L1) > len(L2):
        if len(L2) == 0:
            return 0.0

        for item in L2:
            if item in L1:
                same_widgets += 1
        diff = float(same_widgets) / len(L2)

    return diff


def init(c, t, s, test):
    global countValidFile, train_val, stats
    countValidFile = c
    train_val = t
    train_val['train'] = []
    train_val['val'] = []
    train_val['trainval'] = []
    stats = s
    for t in target_list:
        stats[t] = 0
    test_list = test

if __name__ == '__main__':
    countValidFile = Value('i', 0)
    train_val = Manager().dict()
    stats = Manager().dict()

    folders_list = glob.glob(a.input_dir + input_subdir)
    # folders_list = glob.glob(a.input_dir + "/*20170510_cleaned_outputs*/**")
    folders_list = np.array(folders_list)

    app_nums = []
    for i, app_folder in enumerate(folders_list):
        xmls = glob.glob(app_folder + "/stoat_fsm_output/ui/*.xml")
        pngs = glob.glob(app_folder + "/stoat_fsm_output/ui/*.png")
        app_nums.append(len(pngs))

    remove_list = []
    for i, number in enumerate(app_nums):
        #if number > 500:
        #    remove_list.append(folders_list[i])
        if number == 0:
            remove_list.append(folders_list[i])

    folders_list = [x for x in folders_list if x not in remove_list]

    #create dataset directory
    dir_list = [outputDir, dir_img, dir_set, dir_ann]
    if os.path.exists(outputDir):
        shutil.rmtree(outputDir)
    for d in dir_list:
        os.makedirs(d)

    # train_index = int(len(folders_list) * 4 / 5)
    test_index = np.full(len(folders_list), False)
    for i in range(len(folders_list)):
        if i % 15 == 1:
            test_index[i] = True
    folders_list = np.array(folders_list)
    test_list = folders_list[test_index]

    num_processed = 0
    pool = Pool(processes=1, initializer=init, initargs=(countValidFile, train_val, stats, test_list))
    for r in tqdm(pool.imap(preprocess, folders_list), total=len(folders_list)):
        num_processed += 1
        #print("%s of %s processed" % (num_processed, len(folders_list)))
        with open(os.path.join(dir_set, "train.txt"), 'a+') as f:
            for idx in train_val["train"]:
                f.write("%s\n" % idx)
        
        with open(os.path.join(dir_set, "val.txt"), 'a+') as f:
            for idx in train_val["val"]:
                f.write("%s\n" % idx)
        
        with open(os.path.join(dir_set, "trainval.txt"), 'a+') as f:
            for idx in train_val["trainval"]:
                f.write("%s\n" % idx)
        
        with open(os.path.join(dir_set, "stats.txt"), 'w') as f:
            for t in target_list:
                f.write("{} - {}\n".format(t, stats[t]))

        name_list = []
        for s in r[3]:
            name_list.append(s.split('/')[-1].split('.')[0])
        with open(os.path.join(dir_set, "processed.txt"), 'a+') as f:
            f.write("{}, {}, {}, {}\n".format(r[0], r[1], r[2], name_list))
            # f.write("".join(str(r)).join("\n"))

        train_val["train"] = []
        train_val["val"] = []
        train_val["trainval"] = []
        sys.stdout.flush()
    pool.close()
    pool.join()
